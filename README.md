<h1 align="center">开源三方库资源汇总</h1> 

##### 本文收集了一些已经发布在OHPM(OpenHarmony三方组件中心仓)上的三方组件资源，欢迎应用开发者参考和使用，同时也欢迎开发者将自己开源在OHPM上的三方组件，提PR补充到列表当中。

## <a name="三方组件JS/ArkTS"></a>JS/ArkTS语言

### <a name="OpenHarmoney-TPC"></a>OpenHarmony-TPC
#### <a name="JS-JS/ArkTS"></a>JS/ArkTS语言
##### <a name="UI-自定义控件JS/ArkTS"></a>UI
- [PullToRefresh](https://gitee.com/openharmony-sig/PullToRefresh) - 支持设置内置动画的各种属性，支持设置自定义动画的下拉刷新、上拉加载组件

- [TextLayoutBuilder_bak](https://gitee.com/openharmony-sig/TextLayoutBuilder) - TextLayoutBuilder是一个可定制任意样式的文本构建工具，包括字体间距、大小、颜色、布局方式、富文本高亮显示等

- [overscroll-decor](https://gitee.com/openharmony-sig/overscroll-decor) - UI滚动组件

- [ohos-MPChart](https://gitee.com/openharmony-sig/ohos-MPChart) - mpchart是一个包含各种类型图表的图表库，主要用于业务数据汇总，例如销售数据走势图，股价走势图等场景中使用，方便开发者快速实现图表UI，mpchart主要包括曲线图、柱形图、饼状图、蜡烛图、气泡图、雷达图等自定义图表库

- [material-dialogs](https://gitee.com/openharmony-sig/material-dialogs) - 是自定义对话框库

- [MaterialProgressBar](https://gitee.com/openharmony-sig/MaterialProgressBar) - 是一个自定义ProgressBar效果的库

- [RoundedImageView](https://gitee.com/openharmony-sig/RoundedImageView) - RoundedImageView支持圆角（和椭圆或圆形）的快速 ImageView，它支持许多附加功能，包括椭圆、圆角矩形、ScaleTypes 和 TileModes

- [ohos_banner](https://gitee.com/openharmony-sig/ohos_banner) - 是适配OpenHarmony环境的一款banner库，常用于广告图片轮播场景

- [ohos_highlightguide](https://gitee.com/openharmony-sig/ohos_highlightguide) - 基于OpenHarmony的高亮型新手引导组件，通过高亮区域与蒙版背景的明暗度对比，使用户快速锁定重点功能

- [CircleIndicator](https://gitee.com/openharmony-sig/CircleIndicator) - CircleIndicator是一款UI组件库，为Tabs/Swiper容器提供了多种自定义风格的指示器

- [img2pdf](https://gitee.com/openharmony-tpc/pdfViewer/tree/master) - JavaScript实现jpg、png图片格式添加到pdf的功能。

- [ohos-autofittextview](https://gitee.com/openharmony-sig/ohos-autofittextview) - 自动调整文本大小以完全适合其边界的TextView。

- [RecyclerViewPager](https://gitee.com/openharmony-sig/RecyclerViewPager) - RecyclerViewPager是一个支持自定义左右翻页切换效果、上下翻页切换效果、类似Material风格的容器组件。

- [DanmakuFlameMaster](https://gitee.com/openharmony-sig/DanmakuFlameMaster) - DanmakuFlameMaster是一款弹幕框架，支持发送纯文本弹幕、设置弹幕在屏幕的显示区域、控制弹幕播放状态等功能。

- [ohos-SwipeLayout](https://gitee.com/openharmony-sig/ohos-SwipeLayout) - 支持顶部、底部、左侧、右侧四个方向的滑动布局。

- [shimmer-ohos](https://gitee.com/openharmony-sig/shimmer-ohos) - shimmer是一个简单灵活的为应用视图添加闪烁效果的库，主要有由左到右倾斜，由左到右竖直，由左到右圆形，由上到下水平等闪光效果。

- [MultiType](https://gitee.com/openharmony-sig/MultiType) - 更轻松、更灵活地为 List 创建多种类型布局。

- [WheelPicker](https://gitee.com/openharmony-sig/WheelPicker_bak) - WheelPicker可以实现滚轮选择，通过设置可以实现多种效果，也可以设置属性，改变UI效果，如时间选择器，地区选择器的三级联动，实现需求效果。

- [vlayout](https://gitee.com/openharmony-sig/vlayout_bak) - vlayout能够处理列表、网格和其他布局在同一个视图的复杂情况，使用者可以使用已设定好的容器布局组件，也可以在此基础上自定义容器布局组件。

- [ohos-PickerView](https://gitee.com/openharmony-sig/ohos-PickerView) - 选择器，包括时间选择、地区选择、分割线设置、文字大小颜色设置。

- [SmartRefreshLayout](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/SmartRefreshLayout) - smartrefreshlayout以打造一个强大，稳定，成熟的下拉刷新框架为目标，并集成各种的炫酷、多样、实用、美观的Header。 集成了各种炫酷的 Header。 smartrefreshlayout可包含的信息有：BezierRadar样式、BezierCircle样式、FlyRefresh样式、Classics样式，Phoenix样式，Taurus样式，Taurus样式，HitBlock样式，WaveSwipe等众多样式。



##### <a name="动画JS/ArkTS"></a>动画
- [lottieArkTS](https://gitee.com/openharmony-tpc/lottieETS) - 适用于OpenHarmony的动画库，功能类似于Java组件lottie、AndroidViewAnimations、Leonids等库
  
- [ohos-svg](https://gitee.com/openharmony-sig/ohos-svg) - svg是一个SVG图片的解析器和渲染器，可以解析SVG图片并渲染到页面上，还可以动态改变SVG的样式

- [recyclerview-animators](https://gitee.com/openharmony-sig/recyclerview-animators) - recyclerview_animators是带有添加删除动画效果以及整体动画效果的list组件库。

- [rebound](https://gitee.com/openharmony-tpc/rebound) - rebound是一个模拟弹簧动力学，用于驱动物理动画的库。

- [box2d](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/box2d) - 用于游戏开发，使物体的运动更加真实，让游戏场景看起来更具交互性，比如愤怒的小鸟。

- [d3-ease](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/d3JsEasingDemo) - 该库为UI动画组件 easing 缓动函数是用来描述数值的变化速率，这些数值可以是动画对象的宽高，透明度，旋转，缩放等属性值，它们的变化率可以用函数曲线来表示,制作出更加符合直觉的UI动效,使动画看上去更加真实。

- [LibPag](https://gitee.com/openharmony-sig/ohos_libpag) - PAG（便携式动画图形）文件的官方渲染库，可跨多个平台本地渲染 After Effects 动画。


##### <a name="网络-JS/ArkTS"></a>网络
- [ohos_axios](https://gitee.com/openharmony-sig/ohos_axios) - 一个基于 promise 的网络请求库，可以运行 node.js 和浏览器中。本库基于Axios 原库进行适配，使其可以运行在 OpenHarmony，并沿用其现有用法和特性

- [socketio](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/socketio) - socket.io是一个在客户端和服务器之间实现低延迟、双向和基于事件的通信的库。建立在 WebSocket 协议之上，并提供额外的保证，例如回退到 HTTP 长轮询或自动重新连接。

- [mars](https://gitee.com/openharmony-sig/mars) - Mars 是一个跨平台的网络组件，包括主要用于网络请求中的长连接，短连接，是基于 socket 层的解决方案，在网络调优方面有更好的可控性，暂不支持HTTP协议。 Mars 极大的方便了开发者的开发效率。

- [httpclient](https://gitee.com/openharmony-tpc/httpclient) - HTTP是现代应用程序通过网络交换数据和媒体的的主要方式。httpclient是OpenHarmony 里一个高效执行的HTTP客户端，使用它可使您的内容加载更快，并节省您的流量。httpclient以人们耳熟能详的OKHTTP为基础，整合android-async-http，AutobahnAndroid，OkGo等库的功能特性，致力于在OpenHarmony 打造一款高效易用，功能全面的网络请求库。

- [eventsource](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/eventsource) - eventsource三方库是EventSource客户端的纯JavaScript实现。它提供了一种在客户端与服务器之间建立单向持续连接的机制，服务器可以使用这个连接向客户端发送事件更新，而客户端能够实时接收并处理这些更新。

- [ntp](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ohos_ntp) - ohos_ntp是一个用于网络时间协议(NTP)服务器同步事件的TypeScript库。它允许你的应用程序通过与NTP服务器通信来获取准确的事件信息，以确保你的设备具有准确的系统时钟，或确保能获取到准确时间信息用来调试。

- [ftp-srv](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ohos_ftp-srv) - ftp-srv是一个用于OpenHarmony的Ftp服务器库。通过ftp-srv，开发者可以轻松地创建和管理Ftp服务器，实现文件的上传、下载、目录查看、目录创建、目录删除等操作。

- [xmpp/client](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/xmpp) - @xmpp/client 是一个用于构建基于XMPP（可扩展通讯和即时消息传递协议）的客户端的 库。XMPP是一种开放标准的通信协议，通常用于实现即时通讯和在线实时交流。本库基于xmpp.js原库0.13.1版本进行适配，使其可以运行在 OpenHarmony，并沿用其部分现有用法和特性。



##### <a name="图片JS/ArkTS"></a>图片
- [ImageKnife](https://gitee.com/openharmony-tpc/ImageKnife) - 更高效、更轻便、更简单的图像加载缓存库，能力类似java组件glide、disklrucache、glide-transformations、fresco、picasso、uCrop、Luban、pngj、Android-Image-Cropper、android-crop等库

- [XmlGraphicsBatik](https://gitee.com/openharmony-tpc/XmlGraphicsBatik) - 用于处理可缩放矢量图形（SVG）格式的图像，例如显示、生成、解析或者操作图像

- [ohos_gif-drawable](https://gitee.com/openharmony-sig/ohos_gif-drawable) - 基于Canvas进行绘制,支持gif图片相关功能

- [subsampling-scale-image-view_bak](https://gitee.com/openharmony-sig/subsampling-scale-image-view) - 视图缩放组件

- [ImageViewZoom](https://gitee.com/openharmony-sig/ImageViewZoom) - ImageViewZoom 支持加载 Resource 或 PixelMap 图片，支持设置图像显示类型功能，支持缩放功能，支持平移功能，双击放大功能，可以监听图片大小，资源变化事件，支持清除显示图片功能

- [ThreeJs360Demo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ThreeJs360Demo) - ThreeJs360Demo,使用系统提供的Web组件，加载threeJs，实现360度的全景渲染功能

- [CircleImageView](https://gitee.com/openharmony-sig/CircleImageView) - CircleImageView是一个图片处理的库，可以将图片裁剪为圆形或者给图片设置边框。

- [PhotoView](https://gitee.com/openharmony-sig/PhotoView) - PhotoView是一个图片缩放浏览组件，图片可缩放，平移，旋转。

- [LargeImage](https://gitee.com/openharmony-tpc/LargeImage) - 加载可以执行缩放（放大和缩小）和滚动操作的图像，图像放大之后可以拖动查看。 详细功能： 1.加载图像。 2.支持缩放。 3.支持拖动查看大图。

- [APNG](https://gitee.com/openharmony-sig/ohos_apng) - ohos_apng是以开源库apng-js为参考，基于1.1.2版本，通过重构解码算法，拆分出apng里各个帧图层的数据；使用arkts能力，将每一帧数据组合成imagebitmap，使用定时器调用每一帧数据 通过canvas渲染，从而达到帧动画效果.对外提供解码渲染能力。



##### <a name="多媒体JS/ArkTS"></a>多媒体
- [ohos_ijkplayer](https://gitee.com/openharmony-sig/ohos_ijkplayer) - 一款基于FFmpeg的视频播放器

- [ohos_videocompressor](https://gitee.com/openharmony-sig/ohos_videocompressor) - videoCompressor是一款ohos高性能视频压缩器

- [ohos_video_trimmer](https://gitee.com/openharmony-sig/ohos_video_trimmer) - videotrimmer是在OpenHarmony环境下，提供视频剪辑能力的三方库

- [GSYVideoPlayer](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/GSYVideoPlayer) - GSYVideoPlayer是一个视频播放器库，支持切换内核播放器（IJKPlayer、avplayer），并且支持了多种能力。

- [OhosVideoCache](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/OhosVideoCache) - OhosVideoCache是一个支持播放器边播放边缓存的库，播放器只需要使用OhosVideoCache处理过的url就可以实现音视频的缓存功能。

- [mp4parser](https://gitee.com/openharmony-tpc/mp4parser) - 一个读取、写入操作音视频文件编辑的工具。

- [mp3agic](https://gitee.com/openharmony-sig/mp3agic) - mp3agic 用于读取 mp3 文件和读取/操作 ID3 标签（ID3v1 和 ID3v2.2 到 ID3v2.4）,协助开发者处理繁琐的文件操作相关，多用于操作文件场景的业务应用。

- [metadata-extractor](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/metadata-extractor) - metadata-extractor是用于从图像、视频和音频文件中提取 Exif、IPTC、XMP、ICC 和其他元数据的组件。



##### <a name="数据存储JS/ArkTS"></a>数据存储
- [ohos_disklrucache](https://gitee.com/openharmony-sig/ohos_disklrucache) - 专门为OpenHarmony打造的一款磁盘缓存库，通过LRU算法进行磁盘数据存取

- [ohos_fileio-extra](https://gitee.com/openharmony-sig/ohos_fileio-extra) - 提供了更丰富全面的文件操作功能

- [MMKV](https://gitee.com/openharmony-tpc/MMKV) - 一款小型键值对存储框架

- [dataORM](https://gitee.com/openharmony-sig/dataORM) - dataORM是一个具有一行代码操作数据库或链式调用,备份、升级、缓存等特性的关系映射数据库

- [msgpack-javascript](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/msgpack-javascript) - MessagePack是一个非常高效的对象序列化库

- [arangojs](https://gitee.com/openharmony-sig/arangojs) - 是一款适用于OpenHarmony环境的ArangoDB数据库javascript版驱动

- [protobuf](https://gitee.com/openharmony-tpc/protobuf/tree/1.x/) - ProtoBuf(protocol buffers) 是一种语言无关、平台无关、可扩展的序列化结构数据的方法，它可用于（数据）通信协议、数据存储等

- [node-cache](https://github.com/ptarjan/node-cache) - 内存缓存

- [protobuf_format](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/protobuf_format) - 基于@ohos/protobufjs 1.0.1版本的Message，提供格式转换能力，允许将Message输出重写为基本文本格式，如xml,json,html

- [protobuf](https://gitee.com/openharmony-tpc/protobuf) - ProtoBuf(protocol buffers) 是一种语言无关、平台无关、可扩展的序列化结构数据的方法，它可用于（数据）通信协议、数据存储等。,是一种灵活，高效，自动化机制的结构数据序列化方法比XML更小,更快,更为简单。 本项目主要是OpenHarmony系统下以protobuf.js 7.2.4为主要依赖开发，主要接口针对OpenHarmony系统进行合理的适配研发。

- [avro](https://gitee.com/openharmony-sig/avro) - 实现的数据序列化系统，支持丰富的数据结构，将其转化成便于存储或传输的二进制数据格式。它同时也是一个容器文件，用于存储持久数据。



##### <a name="文件数据JS/ArkTS"></a>文件数据与传输

- [ohos_mqtt](https://gitee.com/openharmony-sig/ohos_mqtt) - 使应用程序能够连接到MQTT代理以发布消息、订阅主题和接收发布的消息。

- [LiveEventBus](https://gitee.com/openharmony-sig/LiveEventBus) - 消息总线，支持Sticky，支持跨进程，支持跨应用广播

- [ohos_mbassador](https://gitee.com/openharmony-sig/ohos_mbassador) - 一个发布订阅模式的三方组件

- [EventBus](http://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/EventBus) - eventbusjs主要功能是消息订阅发送

- [js-sha256](http://github.com/emn178/js-sha256) - sha-256/sha-224 hash算法

- [hi-base32](http://github.com/emn178/hi-base32) - base32 encode/decode

- [js-md5](http://github.com/emn178/js-md5) - A simple MD5 hash function for JavaScript supports UTF-8 encoding.

- [js-sha1](http://github.com/emn178/js-sha1) - A simple SHA1 hash function for JavaScript supports UTF-8 encoding.

- [js-md2](http://github.com/emn178/js-md2) - A simple MD2 hash function for JavaScript supports UTF-8 encoding.

- [brotli](http://gitee.com/openharmony-sig/brotli) - 一种通用无损压缩算法

- [cborjsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/cborjsDemo) - 是OpenHarmony系统下使用cbor-js的示例，cbor-js是以简明二进制对象表示 (CBOR) 数据格式 ( RFC8949 )编码和解析数据的Javascript开源库

- [commons-codec](https://gitee.com/openharmony-tpc/commons-codec) - 是一个OpenHarmony系统下使用各种编解码的示例，包含各种格式的简单编码器和解码器， 例如 Base64 Base32 等除了这些广泛使用的编码器和解码器之外，编解码器包还维护了一组语音编码实用程序

- [pako](https://github.com/nodeca/pako) - pako是一个JavaScript库，支持deflate和gzip压缩解压功能

- [lz4js](https://github.com/Benzinga/lz4js) - Lz4js是一个JavaScript库，实现Lz4压缩/解压库

- [snappyjs](https://github.com/zhipeng-jia/snappyjs) - SnappyJS是一个JavaScript库，实现Snappy压缩/解压库

- [okio](https://gitee.com/openharmony-tpc/okio) - okio是一个通过数据流、序列化和文件系统来优化系统输入输出流的能力的库

- [ohos_jsonwebtoken](https://gitee.com/openharmony-sig/ohos_jsonwebtoken) - JSON Web Token（JWT）是一个开放的行业标准（RFC 7519），它定义了一种简洁的、自包含的协议格式，用于在通信双方传递json对象，传递的信息经过数字签名可以被验证和信任。是一款适用于 openharmony 环境的 Json Web Token实现

- [OpenCSV](https://gitee.com/openharmony-tpc/opencsv) - 现在很多的网站中导出的文件会出现一种csv文件，OpenCSV用于读写CSV文件。


##### <a name="安全-JS/ArkTS"></a>安全
- [crypto-js](https://gitee.com/openharmony-sig/crypto-js) - 加密算法类库，目前支持MD5、SHA-1、SHA-256、HMAC、HMAC-MD5、HMAC-SHA1、HMAC-SHA256、PBKDF2等

- [commons-codec](https://gitee.com/openharmony-tpc/commons-codec) - commons-codec是一个OpenHarmony系统下使用各种编解码的示例，包含各种格式的简单编码器和解码器， 例如 Base64 Base32 等除了这些广泛使用的编码器和解码器之外，编解码器包还维护了一组语音编码实用程序。


##### <a name="工具JS/ArkTS"></a>工具
- [zxing](https://gitee.com/openharmony-tpc/zxing) - 一个解析/生成二维码的组件，能力类似java组件zxing，Zbar、zxing-android-embedded、BGAQRCode-Android等

- [js-tokens](http://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/js-tokens) - js-tokens 是一个微型JavaScript的分词器。小巧的、正则表达式驱动的、宽松的、几乎符合规范的 JavaScript 标记器

- [Easyrelpace](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Easyrelpace) - 使用Levenshtein 距离算法测量两个字符串之间的差异。 Compare Text替换字符串

- [hex-encode-decode](https://github.com/tiaanduplessis/hex-encode-decode) - Hex encode & decode string

- [text-encoding](https://github.com/inexorabletash/text-encoding) - 在JavaScript中对二进制数据的文本数据和类型化数组缓冲区进行编码和解码

- [qr-code-generator](https://gitee.com/openharmony-sig/qr-code-generator) - 二维码生成器

- [juniversalchardet](https://gitee.com/openharmony-sig/juniversalchardet) - 字符编码识别组件

- [text-encoding](https://github.com/zxing-js/text-encoding) - 在JavaScript中对二进制数据的文本数据和类型化数组缓冲区进行编码和解码

- [Adler32Demo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Adler32Demo) - 一个在js环境中实现ADLER-32的校验和算法的三方库

- [dayjs](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/dayjs) - dayjs 是一个轻量的处理时间和日期的 JavaScript 库

- [json-schema](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/json-schema) - jsonschema是一个轻便易用的JSON模式验证器

- [validator](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/validator) - validator.js是字符串验证器和清理器的库

- [percentage-regex](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/percentage-regex) - percentage-regex是百分比验证的库

- [leap-year](https://github.com/sindresorhus/leap-year) - 判断闰年的库

- [time-ampm](https://github.com/ipostol/time-ampm) - 获取24小时时间库

- [node-imgtype](https://github.com/Ackar/node-imgtype) - 获取图片类型库

- [randomColor](https://github.com/davidmerfield/randomColor) - 生成随机颜色的库

- [utilCode](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/utilCode) - utilCode是一个通用工具的示例，包含温度转换、正则校验、图片处理、坐标转换、和颜色获取等常用功能

- [xslt-processor](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/xslt-processor) - 支持使用与XML文档配对的XSLT样式表将XML文档转换成多中文本格式（HTML、Text等）的库

- [pinyin4js](https://gitee.com/openharmony-tpc/pinyin4js) - 一款汉字转拼音的JavaScript开源库

- [Eventmitter3](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/EventEmitter3Demo) - EventEmitter3是一款高性能EventEmitter，支持添加监听事件，监听一次性事件，发送事件，移除事件，统计监听事件的个数，统计监听事件的名称

- [he](https://github.com/mathiasbynens/he) - 支持对字符串进行编解码

- [pcx-js](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/pcx-js) - 提供了PCX图像格式解码的能力

- [isrc_fuse.js](https://gitee.com/pommejason/isrc_fuse.js) - Fuse.js是一款轻量级的JavaScript模糊搜索库，提供了模糊搜索和搜索排序功能

- [amf](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/amf) - amf-convert在javascript环境中可以实现AMF格式的serialization/deserialization

- [compare-versions](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/compare-versions) - 比较两个版本字符串，找出哪个更大、相等或更小。

- [mime](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/mime) - 获取给定文件路径或扩展名的 mime 类型。

- [ohos-jszip](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ohos-jszip) - jszip ，是一个支持创建、编辑以及生成压缩文件的工具库。

- [Hamcrest](https://gitee.com/openharmony-sig/Hamcrest) - hamcrest是匹配器库，可以组合起来匹配。

- [jtar](https://gitee.com/openharmony-sig/jtar) - tar支持tar打包和tar解包功能。

- [dd-plist](https://gitee.com/openharmony-sig/dd-plist) - 解析生成属性列表文件的工具库。

- [mixpanel-ohos](https://gitee.com/openharmony-tpc/mixpanel-ohos) - Mixpanel是一个产品分析工具，它使您能够捕获用户与数字产品交互的数据，并允许您使用简单的交互式报告分析此产品数据，您只需要单击几下就可以查询以及可视化数据。

- [commons-cli](https://gitee.com/openharmony-sig/commons-cli) - commons-cli 是一个命令行解析工具，它可以帮助开发者快速构建启动命令，并且帮助你组织命令的参数、以及输出列表等。

- [xutils](https://gitee.com/openharmony-sig/xutils_bak) - xutils是一个网络、文件、数据库操作的工具库。

- [thrift](https://gitee.com/openharmony-tpc/thrift) - Thrift是一个轻量级的、独立于语言的软件栈，用于点到点RPC实现。Thrift为数据传输、数据序列化和应用程序级处理提供了清晰的抽象和实现。代码生成系统使用一种简单的定义语言作为输入并生成跨编程语言的代码，这些代码使用抽象堆栈来构建可互操作的RPC客户端和服务器。

- [smbj](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/smbj) - 主要用于计算机间共享文件，支持安全保护，访问共享目录、打开文件、读写文件等

- [base64-js](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/base64-js) - base64-js是纯JS中的Base64编码/解码。

- [snakeyaml](https://gitee.com/openharmony-sig/snakeyaml_bak) - snakeyaml语言解析功能库。 支持： Parse & Stringify, YAML Documents等。

- [okdownload](https://gitee.com/openharmony-sig/okdownload) - 可靠，灵活，高性能以及强大的下载引擎。支持单任务下载，多任务串、并行下载，设置多种任务监听等。

- [LibphoneNumber](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/LibphoneNumber) - libphonenumber-js是一个电话号码格式化和解析的Javascript开源库。

- [jwks-rsa](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jwks-rsa) - 从 JWKS（JSON Web 密钥集）端点检索密钥来生成公钥的库，其加密方式采用了非对称公钥加密算法（RSA）和非对称椭圆曲线加密算法(ECC)。

- [jsoup](https://gitee.com/openharmony-sig/jsoup) - 快速且宽容的HTML解析器。

- [ohos-beacon-library](https://gitee.com/openharmony-sig/ohos-beacon-library) - 蓝牙工具，主要涉及信标区域监控以及信标设备测距。

- [js-joda](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/js-joda) - js-joda是一个处理时间日期的工具库，为日期时间类提供了简单API,支持时区，持续时间，日期时间格式化和解析,适配了OpenHarmony的一款不可变日期和时间开源库。

- [icu4j](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/icu4j) - ICU消息字符串解析，intl_messageformat_parser是icu4j指定的依赖库。

- [lodashDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/lodashDemo) - lodash是一个提供拓展功能的JavaScript实用工具库

- [xml2jsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/xml2jsDemo) - 简单的XML到JavaScript对象转换器。它支持双向转换。使用sax-js和xmlbuilder-js。

- [node-csv](https://gitee.com/openharmony-tpc/node-csv) - 该项目使用node-csv解析csv文件，生成csv文件。

- [commonmark](https://gitee.com/openharmony-tpc/commonmark) - Markdown是一种纯文本格式，用于编写结构化文档。CommonMark三方库用于将Markdown格式转换为Html或者xml，以便在网页中显示。

- [RxJS](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/RxJS) -  rxjs是一个通过使用可观察序列来合成异步和基于事件的程序的JavaScript库。

- [appauth](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/appauth) -  此库可使用OAuth 2.0和OpenID Connect对用户进行身份验证和授权，它还支持对OAuth 的PKCE扩展。

- [node-polyfill](https://gitee.com/openharmony-sig/ohos_polyfill) -  该项目是一个 polyfill，包含 NodeJs 部分模块的子集。用于 npm 仓中 nodejs build-in 基础模块的 api 适配。目前包括如下模块部分接口 Node Apis。




##### <a name="其他JS/ArkTS"></a>其他

- [arouter-api-onActivityResult](https://gitee.com/openharmony-tpc/arouter-api-onActivityResult) - 用于在各种应用或页面间的跳转和页面间的数据传递

- [ahocorasick](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ahocorasick) - ahocorasick是Aho-Corasick字符串搜索算法的实现，能够高效的进行字符串匹配

- [bignumber.js](https://github.com/MikeMcl/bignumber.js) - A JavaScript library for arbitray-precision decimal and non-decimalarithmetic

- [jsDiffDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jsDiffDemo) - JavaScript文本差异的工具库

- [is-png](https://gitee.com/openharmony-sig/is-png) - is-png是一个判断图片格式的库，根据图片的文件数据，判断图片是否为png格式

- [is-webp](https://gitee.com/openharmony-sig/is-webp) - is-webp是一款根据文件数据，判断图片是否是webp格式的库

- [jmustache](https://gitee.com/openharmony-sig/jmustache) - 是mustache模板系统的零依赖实现，通过使用散列或对象中提供的值来扩展模板中的标签

- [leven](https://github.com/sindresorhus/leven) - Measure the difference between two strings using the Levenshtein distance algorithm

- [caverphone](https://github.com/tcort/caverphone) - A JavaScript implementation of the Caverphone 2.0 (aka Caverphone Revised) phonetic matching algorithm

- [metaphone](https://github.com/words/metaphone) - Metaphone phonetic algorithm

- [behaviorTree](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/behaviorTree) - 是行为树 javascript 版实现

- [ohos_coap](https://gitee.com/openharmony-tpc/ohos_coap) - ohos_coap是基于libcoap v4.3.1版本，封装napi接口，给上层ts提供coap通信能力的三方库

- [gcoord](https://github.com/hujiulong/gcoord) - gcoord(geographic coordinates)是一个处理地理坐标系的JS库，用来修正百度地图、高德地图及其它互联网地图坐标系不统一的问题

- [CaverPhone](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/CaverPhone) - CaverPhone算法(语音匹配算法)的JavaScript实现，规则为：将关键字转换为小写，移除不是a-z的字符，按照规则替换指定字符(如字符串起始、结束，文本中包含cq等)，在结尾放置6个1，返回前十个字符，具体参照CaverPhone算法规则

- [Adler32Demo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Adler32Demo) - 是一个在js环境中实现ADLER-32的校验和算法能力的库

- [bignumberjsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/bignumberjsDemo) - bignumber.js是一个数学库，用于任意精度十进制和非十进制算术的 JavaScript 库

- [aki](https://gitee.com/openharmony-sig/aki) - 是一款边界性编程体验友好的ArkTs FFI开发框架，针对OpenHarmony Native开发提供JS与C/C++跨语言访问场景解决方案

- [jama](https://gitee.com/openharmony-sig/jama) - 基本线性代数包，用于构造和操作真实密集矩阵的库

- [Checksum](https://gitee.com/openharmony-sig/checksum) - 用于计算最典型的散列函数，如 md5 或 sha1

- [VCard](https://gitee.com/openharmony-tpc/VCard) - VCard是电子名片的文件格式标准。它一般附加在电子邮件之后，但也可以用于其它场合（如在网际网路上相互交换）。 VCard可包含的信息有：姓名、地址资讯、电话号码、URL，logo，相片等。本库支持VCard标准2.0和3.0。

- [epublib](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/epublib) - Epublib是一个用于读取/写入/操作epub文件的ets库。

- [flexsearch-ohos](https://gitee.com/openharmony-tpc/flexsearch-ohos) - FlexSearch 是一个快速、零依赖的全文搜索库。 在原始搜索速度方面，FlexSearch 优于每一个搜索库， 并提供灵活的搜索功能，如多字段搜索，语音转换或部分匹配。根据使用的选项，它还提供最高内存效率的索引。 FlexSearch 引入了一种新的评分算法，称为“上下文索引”，基于预先评分的词典字典体系结构，与其他库相比，实际执行的查询速度有大幅度提高。 FlexSearch 还为您提供非阻塞异步处理模型，以通过专用平衡线程并行地对索引执行任何更新或查询。

- [logback](https://gitee.com/openharmony-sig/logback) - 日志记录框架。

- [jchardet](https://gitee.com/openharmony-sig/jchardet) - jchardet是一个检测文本文件(字节流)编码方式的ArkTs实现。

- [nodeRulesDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/nodeRulesDemo) - node-rules 是一个轻量级的正向链接规则引擎。

- [ohos_gson](https://gitee.com/openharmony-sig/ohos_gson) - Gson用于对象与JSON字符串之间的互相转换，并支持JsonElement对象类型，使JSON字符串与对象之间的转换更高效、灵活，并且易于使用。

- [reflect-metadata](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/reflect-metadata) - reflect_metadata用于在TypeScript中操作类的元数据，允许在声明类和属性时添加和读取元数据。

- [jackrabbit](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jackrabbit) - 支持AMQP（Advanced Message Queuing Protocol）网络通信协议的library，可以在一个进程间传递异步消息。 Jackrabbit底层依赖amqplib库，在RabbitMQ上实现了多种消息传递模式。

- [RocketChat](https://gitee.com/openharmony-tpc/RocketChat) - RocketChat指的是一系列服务器方法和消息订阅的应用程序接口集合。使用本库第三方应用程序可以通过REST API控制和查询RocketChat服务器。本库专为聊天自动化而设计，使应用程序开发人员能够轻松地为其社区提供最佳解决方案和体验。

- [ohos_smack](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ohos_smack) - Smack是一个基于XMPP协议的一个聊天客户端。

- [stun-server](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/stun-server) - stun是基于STUN协议的服务开源组件，它允许客户端获取NAT分配的外部IP地址和端口号，还可以识别NAT的行为类型。

- [sanitize-html](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/sanitize-html) - sanitize-html 提供了HTML清理API，支持HTML片段清理。内置默认的标签、属性等、可根据默认属性进行HTML清理，同时也可进行自行配置，根据用户需求进行自定义HTML清理规则。

- [ohos_commons-fileupload](https://gitee.com/openharmony-sig/ohos_commons-fileupload) - commons-fileupload 是一个请求库。可以用来做文件上传（支持分片）、基本请求、文件下载。

- [FastBle](https://gitee.com/openharmony-sig/FastBle) - FastBle是一个处理蓝牙BLE设备的库，可以对蓝牙BLE设备进行过滤，扫描，连接，读取，写入等。

- [asn1Demo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/asn1Demo) - 构建ASN.1对象模型以及JSON序列化/反序列化以及DER 序列化/反序列化。

- [retrofit](https://gitee.com/openharmony-tpc/retrofit) - 一款用于 OpenHarmony平台的HTTP客户端。

- [mathjsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/mathjsDemo) - mathjs是一个OpenHarmony系统下使用数学运算的示例，包含数字、大数、三角函数、字符串、和矩阵等数学功能。


#### <a name="三方组件C_CPP"></a>C/C++语言

[C/C++三方库资源汇总](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/docs/thirdparty_list.md)


##### <a name="音视频C_CPP"></a>音视频
- [vorbis](https://gitee.com/openharmony-sig/vorbis) [GN编译] - 一种通用音频和音乐编码格式组件
- [opus](https://gitee.com/openharmony-sig/opus) [GN编译] - Opus是一个开放格式的有损声音编码格式
- [flac](https://gitee.com/openharmony-sig/flac) [GN编译] - 无损音频编解码器
- [lame](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lame) [交叉编译] - 是开源mp3编码库，使用MPGLIB解码引擎，专门用于编码 mp3
- [FFmpeg](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/FFmpeg) [交叉编译] - 是领先的多媒体框架，能够解码、编码、转码、mux、demux、流式传输、过滤和播放人类和机器创建的任何东西， 可以运行在linux，mac，windows等平台；
- [alsa-lib](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/alsa-lib) [交叉编译] - 是一个用于操作音频设备的库，它提供了一组API，使得应用程序可以与音频设备进行交互
- [faad2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/faad2) [交叉编译] - 是一个音频解码的库
- [fdk-aac](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/fdk-aac) [交叉编译] - 是一个开源的AAC编码库，被认为是开源AAC编码器中音质最好的之一。它支持多种编码模式，包括LC-AAC、HE-AAC和HE-AAC V2
- [libdash](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libdash) [交叉编译] - 是ISO/IEC MPEG-DASH标准的官方参考软件，为Bitmovin开发的MPEG-DASH提供面向对象（OO）接口
- [libid3tag](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libid3tag) [交叉编译] - 是一个用于读取和写入ID3标签的库
- [libmad](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libmad) [交叉编译] - MPEG 音频解码器库
- [libmediasoupclient](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libmediasoupclient) [交叉编译] - 是一个C++客户端库，用于构建基于 mediasoup 的应用程序
- [libmp3lame](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libmp3lame) [GN编译] - 是开源mp3编码库，使用MPGLIB解码引擎，专门用于编码 mp3
- [openal-soft](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openal-soft) [交叉编译] - OpenAL Soft是OpenAL 3D音频API的软件实现
- [pvmp3dec](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/pvmp3dec) [交叉编译] - 是一个开源的MP3解码库
- [sonic](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/sonic) [交叉编译] - 是一种用于加速或减慢音频算法的库
- [soundtouch](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/soundtouch) [交叉编译] - 是一个提供音频变速变调能力的库
- [soxr](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/soxr) [交叉编译] - 是一个重采样库，执行一维采样率转换，例如，可用于对 PCM 编码的音频进行重采样
- [speechd](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/speechd) [交叉编译] - Speech Dispatcher项目提供了一个高级的独立于设备的层，用于通过一个简单、稳定且文档良好的接口访问语音合成
- [srs](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/srs) [交叉编译] - 是一个简单高效的实时视频服务器，支持RTMP/WebRTC/HLS/HTTP-FLV/SRT/GB28181
- [tagLib](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/taglib) [交叉编译] - 是一个用于读取和编辑几种流行音频格式的元数据的库
- [uavs3d](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/uavs3d) [交叉编译] - 是一个开源和跨平台的avs3解码器
- [vid.stab](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/vid.stab) [交叉编译] - 是一个处理视频抖动的库
- [libvpx](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libvpx) [交叉编译] - 是支持vp8、vp9编码解码的开源软件


##### <a name="加解密算法C_CPP"></a>加解密算法
- [libogg](https://gitee.com/openharmony-sig/libogg) [GN编译]- 编解码器
- [libsodium](https://gitee.com/hihopeorg/libsodium) [GN编译]- 易用，可移植的加解密库
- [cryptopp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cryptopp) [交叉编译] - 是密码学库，集成了非常多的密码算法
- [phf](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/phf)  [交叉编译] - 是实现完美hash算法的库
- [DBoW2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/DBoW2) [交叉编译] - 是一种基于词袋模型的图像特征处理和匹配算法
- [GmSSL](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/GmSSL) [交叉编译] - 是支持国密算法和标准的 OpenSSL 分支，增加了对国密 SM2/SM3/SM4 算法和 ECIES、CPK、ZUC 算法的支持，实现了这些算法与 EVP API 和命令行工具的集成
- [BoringSSL](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/boringssl) [交叉编译] - 是OpenSSL的一个分支，旨在满足Google的需求
- [fribidi](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/fribidi) [交叉编译] - 是Unicode双向算法的实现库
- [gmp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/gmp) [交叉编译] - 是用于任意精度算术的运算库
- [kaldi](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/kaldi) [交叉编译] - 是开源语音识别工具(Toolkit)，它使用WFST来实现解码算法
- [fftw3](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/fftw3) [交叉编译] - 是一个快速计算离散傅里叶变换的标准C语言程序集
- [kissfft](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/kissfft) [交叉编译] - 是提供快速傅立叶变换算法能力的库
- [liblinear](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/liblinear) [交叉编译] - 是一个用于解决线性规划、回归算法和异常检测的库
- [libsvm](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libsvm) [交叉编译] - 是一个支持向量机的库
- [marisa](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/marisa-trie) [交叉编译] - 递归存储匹配算法种静态的、节省空间的trie数据结构
- [md5](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/md5-c) [交叉编译] - 的全称是md5信息摘要算法，用于确保信息传输的完整一致
- [openfst](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openfst) [交叉编译] - 是一个开源的有限状态转换（FST）库，主要用于构建、操作和分析有限状态自动机和有限状态转换
- [openssl](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openssl) [交叉编译] - 是一个强大的、商业级的、功能齐全的用于传输层安全（TLS）协议的开源工具包，以前称为安全套接字层（SSL）协议，应用程序可以使用这个包来进行安全通信，避免窃听，同时确认另一端连接者的身份
- [openssl_quic](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openssl_quic) [交叉编译] - 是openssl加密库的一个分支用于启用quic
- [polarssl](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/polarssl) [交叉编译] - 库是SSL和TLS协议以及各自加密算法的实现
- [sha](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/sha) [交叉编译] - 是计算出一个数字消息所对应到的，长度固定的字符串（又称消息摘要）的算法
- [tink](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tink) [交叉编译] - 是一个多语言、跨平台的库，提供安全、易于正确使用且难以滥用的加密API
- [xxHash](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/xxHash) [交叉编译] - 是一种极快的哈希算法，在RAM速度限制下处理。 代码具有高度可移植性


##### <a name="图像图形处理C_CPP"></a>图像图形处理
- [stb-image](https://gitee.com/openharmony-sig/stb-image)  [GN编译]  - C/C++实现的图像解码库
- [pyclipper](https://gitee.com/openharmony-sig/pyclipper)  [GN编译] - 图形处理库，可以用于解决平面二维图形的多边形简化、布尔运算和偏置处理
- [jbig2enc](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jbig2enc)  [GN编译] -  是JBIG2文件的编码器
- [leptonica](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/leptonica)  [交叉编译] -  一个开放源码的C语言库，它被广泛地运用于图像处理和图像分析
- [openjpeg](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openjpeg) [交叉编译] -  是用 C 语言编写的开源 JPEG 2000 编解码器
- [tiff](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tiff) [交叉编译] -  是一个用来读写标签图片(tiff)的库。该库还支持如下文件格式的转化
- [jasper](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/jasper) [交叉编译] - 是一个用于图像编码和操作的软件集合
- [tinyexr](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/tinyexr) [交叉编译] - 是加载和保存OpenEXR(.exr) 映像的小型库
- [Chipmunk2D](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/Chipmunk2D) [交叉编译] - 是一个在MIT许可下分发的2D刚体物理库
- [Anti-Grain Geometry](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/agg) [交叉编译] - 是一个用C++编写的开源二维矢量图形库
- [cairo](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cairo) [交叉编译] - 是一个2D图形库，支持多种输出设备
- [exiv2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/exiv2) [交叉编译] - 是一个C++库和命令行实用程序，用于读取、写入、删除和修改Exif、IPTC、XMP和ICC图像元数据
- [Clipper Library](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/clipper) [交叉编译] - 提供了对线段和多边形的裁剪(Clipping)以及偏置(offseting)的功能
- [clip2tri](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/clip2tri) [交叉编译] - 是使用clipper和poly2tri一起进行鲁棒三角剖分的三方库
- [earcut.hpp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/earcut.hpp) [交叉编译] - 是一个c++版本的earcut.js，一个快速的，只有头文件的多边形三角测量库
- [geos](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/geos) [交叉编译] - 是一个C++库，用于对二维矢量几何图形执行操作
- [giflib](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/giflib) [交叉编译] - 是一个用于阅读和编写gif图像的库
- [glm](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/glm) [交叉编译] - OpenGL Mathematics（GLM）是一个基于OpenGL着色语言（GLSL）规范的图形软件的仅限标题的C++数学库
- [jbig2dec](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jbig2dec) [交叉编译] - 是JBIG2图像压缩格式的解码器实现
- [jbigkit](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jbigkit) [交叉编译] - 是JBIG1数据压缩标准（ITU-T T.82）的软件实现，该标准是为扫描文档等双层图像数据设计的
- [jpeg](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jpeg) [交叉编译] - 是JPEG图像压缩免费库
- [lcms2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lcms2) [交叉编译] - 是一个色彩管理库，实现ICC配置文件之间的快速转换。它专注于速度，并且可以跨多个平台移植
- [libavif](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libavif) [交叉编译] - 用于编码和解码avif格式图像文件
- [libheif](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libheif) [交叉编译] - 是HEIF和AVIF文件格式编解码三方库
- [libjpeg-turbo](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libjpeg-turbo)  [交叉编译] - 是一种JPEG图像编解码器，它使用SIMD指令来加速基准JPEG压缩和解压缩
- [libpng](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libpng) [交叉编译] - 是一款C语言编写的用来读写PNG文件的库
- [libtess2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libtess2) [交叉编译] -  可以对复杂多边形进行曲面细分
- [libvips](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libvips) [交叉编译] - 是一个需求驱动的水平线程图像处理库
- [libwebp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libwebp) [交叉编译] -  编解码器是一个用于编码和解码 WebP 格式图像的库。该软件包包含可用于其他程序以添加WebP支持的库，以及分别用于压缩和解压缩图像的命令行工具“cwebp”和“dwebp”
- [libyuv](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libyuv) [交叉编译] - 是一个开源的图像处理库，提供了多种图像处理功能，包括图像格式转换、颜色空间转换、颜色调整、去噪、去雾、锐化、缩放等
- [pixman](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/pixman) [交叉编译] - 是一个用于像素操作的低级软件库，提供图像合成和梯形光栅化等功能
- [stb](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/stb) [交叉编译] - 是一个图像读写库
- [opencv](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/opencv) [交叉编译] - 是一个开源计算机视觉和机器学习软件库,提供了丰富的图像处理和分析功能，适用于各种应用领域

##### **<a name="动画C_CPP"></a>动画**

- [DragonBones](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/DragonBonesCPP) [交叉编译] -  是一套骨骼动画工具集，最早使用 Flash 和 ActionScript 3.0 语言开发，主要在 Flash 游戏中使用，目前在页游和手游项目中使用很广泛
- [spine-runtimes](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/spine-runtimes) [交叉编译] - 项目托管了Spine运行时。


##### <a name="网络协议通信C_CPP"></a>网络协议通信
- [nanopb](https://gitee.com/hihopeorg/nanopb) [GN编译] - 轻量的支持C语言的一种数据协议，可用于数据存储、通信协议等方面
- [c-ares](https://gitee.com/openharmony-sig/c-ares) [交叉编译] - 异步解析器库，适用于需要无阻塞地执行 DNS 查询或需要并行执行多个 DNS 查询的应用程序
- [libevent](https://gitee.com/openharmony-sig/libevent) [GN编译] - 事件通知库
- [kcp](https://gitee.com/openharmony-sig/kcp) - ARQ 协议,可解决在网络拥堵情况下tcp协议的网络速度慢的问题
- [mqtt](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/mqtt) [交叉编译] - MQTT 是用 C 语言编写的用于MQTT协议的Eclipse Paho C客户端库
- [nghttp3](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/nghttp3) - 是在C中通过QUIC和QPACK进行HTTP/3映射的实现，它不依赖于任何特定的QUIC传输实现
- [axTLS](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/axtls) [交叉编译] - 嵌入式 SSL 项目是一个高度可配置的客户端/服务器 TLSv1.2 库，专为内存需求较小的平台而设计,它带有一个小型HTTP / HTTPS服务器和其他测试工具
- [coturn](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/coturn) [交叉编译] - 是TURN和STUN服务器的免费开源实现
- [cpp-httplib](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cpp-httplib) [交叉编译] - 是一个C++11单文件头跨平台HTTP/HTTPS库
- [curl](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/curl) [交叉编译] - 是一个C库用于网络请求
- [exosip](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/exosip) [交叉编译] - 库是osip2的扩展库，它隐藏了使用SIP协议建立多媒体会话的复杂性
- [iperf](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/iperf) [交叉编译] - 是一个网络性能测试工具，iperf可以测试TCP和UDP带宽质量。iperf即可测量最大TCP带宽，也具有多种参数和UDP特性，且可报告带宽，延迟抖动和数据包丢失
- [iproute2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/iproute2) [交叉编译] - 是一个Linux操作系统中的工具集，用于网络栈的配置、监控和管理。它提供了一组命令行工具和一套API，用于处理数据包路由、网络设备、网络地址和协议。
- [cyrus-sasl](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cyrus-sasl) [交叉编译] - 简单身份验证和安全层 （SASL） 是一种规范，用于描述如何将身份验证机制插入到网络上的应用程序协议中。Cyrus SASL 是 SASL 的一种实现，它使应用程序开发人员可以轻松地以通用方式将身份验证机制集成到其应用程序中
- [libical](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libical) [交叉编译] - 是一个开源实现关于iCalendar协议和协议数据单元
- [libosip2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libosip2) [交叉编译] - 是一个开放源代码的sip协议栈,是开源代码中不多使用C语言写的协议栈之一,它具有短小简洁的特点,专注于sip底层解析使得它的效率比较高
- [libpcap](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libpcap)  [交叉编译] - 是unix/linux平台下的网络数据包捕获函数包，大多数网络监控软件都以它为基础
- [minidlna](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/minidlna) [交叉编译] -（又名ReadyDLNA）是服务器软件，旨在完全兼容DLNA / UPnP-AV客户端
- [modbus](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/modbus) [GN编译] - 是用 C 语言编写的第三方Modbus库来实现modbus通讯，支持 RTU（串行）和 TCP（以太网）通信模式，可根据 Modbus 协议发送和接收数据
- [nghttp2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/nghttp2) [交叉编译] - 是一个实现http2超文本传输协议的C
- [nghttp3](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/nghttp3) [交叉编译] - 是在C中通过QUIC和QPACK进行HTTP/3映射的实现，它不依赖于任何特定的QUIC传输实现
- [ngtcp2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/ngtcp2) [交叉编译] - 项目是实现RFC9000 QUIC协议
- [libsrtp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libsrtp) [交叉编译] - 提供了保护RTP和RTCP的功能。RTP数据包可以进行加密和身份验证（使用srtp_protect（）函数），将其转换为srtp数据包
- [ortp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/ortp) [交叉编译] - 是一个实现RTP协议的C库
- [pjsip](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/pjsip)  [交叉编译] - 是一个免费的开源多媒体通信库，以C语言编写，实现了基于标准的协议，如SIP、SDP、RTP、STUN、TURN和ICE。它将信令协议（SIP）与丰富的多媒体框架和NAT穿透功能相结合，形成高级API
- [pupnp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/pupnp) [交叉编译] - 是提供UPnP协议能力的三方库
- [thrift](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/thrift) [交叉编译] - 是一种接口描述语言和二进制通讯协议，它被用来定义和创建跨语言的服务
- [websocketpp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/websocketpp) [交叉编译] - 是c++ websocket客户端/服务器库
- [libiscsi](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libiscsi) [交叉编译] - 是一个客户端库，用于实现iSCSI协议，可用于访问iSCSI目标的资源


##### <a name="数据压缩解压C_CPP"></a>数据压缩解压

- [lzma](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lzma) [GN编译] - 是2001年以来得到发展的一个数据压缩算法，它是一种高压缩比的传统数据压缩软件 
- [zstd](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/zstd) [交叉编译] -  一种快速的无损压缩算法，是针对 zlib 级别的实时压缩方案，以及更好的压缩比 
- [minizip-ng](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/minizip-ng) [交叉编译] \- 一个用C编写的zip文件操作库 
- [unrar](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/unrar) [交叉编译] - 一个解压rar文件的库
- [xz](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/xz) [交叉编译] - 是免费的通用数据压缩软件，具有较高的压缩比
- [WavPack](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/WavPack) [交叉编译] - 是一个无损音频压缩的库
- [brotli](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/brotli) [交叉编译] - 程序库是一个Jyrki Alakuijala和Zoltán Szabadka开发的开源数据压缩程序库
- [bzip2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/bzip2) [交叉编译] - 是使用 Burrows–Wheeler 算法，压缩解压文件
- [djvulibre](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/djvulibre) [交叉编译] - 是一组压缩技术，一种文件格式，以及用于通过网络递送数字文档的软件平台，扫描文档和高分辨率图像
- [libzip](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libzip) [交叉编译] - 是一个用于读取、创建和修改zip存档的C库。文件可以从数据缓冲区、文件或直接从其他zip档案复制的压缩数据中添加
- [p7zip](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/p7zip) [交叉编译] - 是一个功能齐全的压缩打包应用
- [snappy](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/snappy)  [交叉编译] - 是一个压缩/解压缩库。它的目标不是最大化 压缩，或与任何其他压缩库兼容;相反 它的目标是非常高的速度和合理的压缩。例如 与 zlib 的最快模式相比，Snappy 快了一个数量级 对于大多数输入，但生成的压缩文件从 20% 到 100%更大
- [tremolo](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tremolo) [交叉编译] - 是xiph.org上Tremor lib的ARM优化版本。Tremor库是一个用于执行Ogg Vorbis解压缩的纯整数库
- [unzip](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/unzip) [交叉编译] - 为zip压缩文件的解压缩程序
- [zlib](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/zlib) [交叉编译] - 是提供数据压缩用的函式库


##### <a name="文本解析器C_CPP"></a>文本解析器

- [xerces-c](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/xerces-c) [交叉编译] - 一个开放源代码的XML语法分析器，它提供了SAX和DOM API
- [rapidjson](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/rapidjson) [GN编译]- 一个跨平台的c++的json的解析器和生成器
- [tinyxml2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tinyxml2) [交叉编译] - 是 simple、small、efficient 的开源 C++ XML 文件解析库
- [tinyxpath](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tinyxpath) [交叉编译]- 用于从 XML 树中提取 XPath 1.0 表达式
- [json-schema-validator](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/json-schema-validator) [交叉编译] - 用于验证基于JSON Schema的JSON文档
- [libxlsxwriter](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/libxlsxwriter) [交叉编译] - 是一个可以向Excel写入文字和图片的库
- [pugixml](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/pugixml)  [交叉编译] - 是一个C++XML处理库
- [HDiffPatch](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/HDiffPatch) [交叉编译] - 是一个用于比较和合并文本差异的C++库
- [LuaXML](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/LuaXML) [交叉编译] - 作为一个开源项目，是在Lua和XML之间建立映射的模块
- [cJSON](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cJSON) [交叉编译] - 是使用C语言编写，用来创建、解析JSON文件的库。cJSON特点就是工程文件简单，只有一个.c和一个.h，但提供函数接口功能齐全，麻雀虽小五脏俱全，使得在嵌入式工程中使用起来得心应手
- [json-c](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/json-c) [交叉编译] - 是json数据解析库
- [json](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/json) [交叉编译] - 是一个C++的处理json数据解析的库
- [jsoncpp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jsoncpp) [交叉编译] - 是一个C++库，允许操作JSON值，包括对字符串的序列化和反序列化
- [libexpat](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libexpat) [交叉编译] - 是一个用于解析XML 1.0的C99库，面向流的XML解析器
- [libunibreak](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libunibreak) [交叉编译] - 是一个文本处理器
- [libxls](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libxls) [交叉编译] - 是一个解析Excel表格的库
- [libxml2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libxml2) [交叉编译] - 是一个用于解析XML文档的C语言库。它提供了一组API，可以用于读取、修改和创建XML文档
- [libxslt](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libxslt) [交叉编译] - 库用于处理和转换XML文档
- [libexif](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libexif) [交叉编译] - 是一个用于解析、编辑和保存EXIF数据的库
- [miniini](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/miniini) [交叉编译] - 是一个用于解析INI文件的库
- [pcre2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/pcre2) [交叉编译] - 是升级版的支持 Perl 语法的正则表达式库，能够用于处理文本匹配、搜索和替换等操作
- [rapidjson](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/rapidjson) [GN编译] - 是一个跨平台的c++的json的解析器和生成器
- [xmlrpc-c](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/xmlrpc-c) [交叉编译] - 是通过Internet进行过程调用的一种快速简便的方法，它将过程调用转换为XML文档，使用HTTP将其发送到远程服务器，并以XML形式获取响应


##### <a name="编码转换C_CPP"></a>编码转换

- [ Libiconv](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/iconv) [GN编译] - 一个实现字符集转换的库，用于没有Unicode或无法从其他字符转换为Unicode的系统
- [Jansson](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jansson) [交叉编译] - 是一个用于解码、编码、操控JSON的C库
- [libqrencode](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libqrencode) [交叉编译] - 是一个快速紧凑的库，用于将数据编码为二维码，二维码是一种二维符号，可以通过智能手机等方便的终端进行扫描
- [libuuid](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libuuid) [交叉编译] - 生成唯一识别码
- [lua-amf3](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lua-amf3) [交叉编译] - 为lua提供AMF二进制格式数据编解码功能

##### **<a name="框架类C_CPP"></a>框架类**

- [ CUnit](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/CUnit) [交叉编译] - 是C语言的单元测试框架
- [InferLLM](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/InferLLM) [交叉编译] - 是一个简单高效的 LLM CPU 推理框架，可以实现在本地部署 LLM 中的量化模型
- [MNN](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/MNN) [交叉编译] - 是一个高效、轻量级的深度学习框架,支持深度学习模型的推理和训练
- [apr](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/apr) [交叉编译] - 是一个是创建和维护软件库，提供一组映射到下层操作系统的API
- [assimp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/assimp) [交叉编译] - 作为一个开源项目，设计了一套可扩展的架构，为模型的导入导出提供了良好的支持
- [bcunit](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/bcunit) [交叉编译] - 是一个单元测试框架
- [behaviortree](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/behaviortree) [交叉编译] - 提供了一个创建行为树的框架，常用来做任务或状态管理
- [Caffe](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/caffe) [交叉编译] - 一个用于深度学习的快速开放框架
- [chrono](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/chrono) [交叉编译] - 是一个多物理场开源框架，用于多物理场和多体动力学仿真的高性能C++库
- [googletest](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/googletest) [交叉编译] - 是Google提供的一套单元测试框架
- [nspr](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/nspr) [交叉编译] - Netscape Portable Runtime（NSPR）为系统级和类似libc的函数提供了一个平台-中间API。API用于Mozilla客户端、许多Red Hat和Oracle的服务器应用程序以及其他软件产品
- [uchardet](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/uchardet) [交叉编译] - 是一个编码检测器库，它采用未知字符编码的字节序列，不带任何附加信息，并尝试确定文本的编码。返回的编码名称与图标兼容

##### **<a name="数据结构存储C_CPP"></a>数据结构存储**

- [RingBuffer](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/RingBuffer) [交叉编译] - 是一个简单易用的环形缓冲库
- [avro](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/avro) [交叉编译] - 是指数据序列化的系统，有丰富的数据结构类型、快速可压缩的二进制数据形式
- [avrocpp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/avrocpp) [交叉编译] - 是指数据序列化的系统，有丰富的数据结构类型、快速可压缩的二进制数据形式
- [flatbuffers](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/flatbuffers) [交叉编译] - 是一个跨平台的序列化库，其架构能最大限度地提高内存效率
- [hdf5](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/hdf5) [交叉编译] - 是一种常见的跨平台数据储存文件，可以存储不同类型的图像和数码数据，并且可以在不同类型的机器上传输，同时还有统一处理这种文件格式的函数库
- [protobuf](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/protobuf) [交叉编译] - 是Google提供的一套数据的序列化框架
- [protobuf_v3.6.1](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/protobuf_v3.6.1) [交叉编译] - (Protocol Buffers)是一种跨平台、语言无关、可扩展的序列化结构数据的方法，可用于网络数据交换及存储
- [sqlite](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/sqlite) [交叉编译] -  是一个提供数据库操作能力的库
- [sqliteodbc](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/sqliteodbc) [交叉编译] - 是基于SQLite数据库的ODBC驱动程序
- [unixODBC](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/unixODBC) [交叉编译] - 项目的目标是开发和推广unixODBC，使其成为非MS Windows平台上ODBC的最终标准
- [sqlcipher](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/sqlcipher) [交叉编译] -  是 SQLite 的一个独立分支，它增加了数据库文件的 256 位 AES 加密和其他安全功能

##### **<a name="深度学习C_CPP"></a>深度学习**

- [Paddle-Lite](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/Paddle-Lite) [交叉编译] -  是一个高性能的深度学习引擎，支持移动端和边缘设备
- [mxnet](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/mxnet)  [交叉编译] - 是一个轻量级、便携、灵活的分布式/移动深度学习框架
- [oneDNN](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/oneDNN) [交叉编译] - 是为深度学习应用开发的一块跨平台开源库

##### <a name="字体C_CPP"></a>字体字幕处理

- [fontconfig](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/fontconfig) [交叉编译] - 是字体相关的计算机程序库，用于配置、定制全系统的字体，或将字体提供给应用程序使用
- [freeType](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/freetype2) [交叉编译] - 是用C语言编写的。它设计为小巧、高效且高度可定制，同时能够为数字排版生成大多数矢量和位图字体格式的高质量输出（字形图像）,FreeType是一个免费提供的便携式软件库，用于渲染字体
- [harfbuzz](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/harfbuzz) [交叉编译] - 是一个有OpenType文本整形能力的库
- [libass](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libass) [交叉编译] - 库则是一个轻量级的对ASS/SSA格式字幕进行渲染的开源库

##### <a name="日志打印C_CPP"></a>日志打印

- [log4cplus](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/log4cplus) [交叉编译] - 是一个简单易用的C++日志记录API，它对日志管理和配置提供了线程安全、灵活和任意粒度的控制
- [glog](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/glog) [交叉编译] - 提供基于C++样式流的日志记录API
- [cups](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cups)  [交叉编译] - 是苹果公司为macOS®和其他类似UNIX®的操作系统开发的基于标准的开源打印系统


##### <a name="其他工具类C_CPP"></a>其他工具类

- [lua](https://gitee.com/openharmony-sig/lua)  [GN编译] - Lua是一种功能强大、高效、轻量级、可嵌入的脚本语言
- [inotify-tools](https://gitee.com/hihopeorg/inotify-tools)  [GN编译] - 异步文件系统监控组件，它满足各种各样的文件监控需要，可以监控文件系统的访问属性、读写属性、权限属性、删除创建、移动等操作
- [libharu](https://gitee.com/openharmony-sig/libharu) [GN编译] - 用于生成 PDF格式的文件
- [leveldb](https://gitee.com/openharmony-sig/leveldb) [GN编译] - 快速键值存储库，提供从字符串键到字符串值的有序映射
- [bsdiff](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/bsdiff) [交叉编译] - 一个提供二进制文件拆分以及合并能力的三方组件
- [concurrentqueue](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/concurrentqueue) [交叉编译] - 一个高效的线程安全的队列的库
- [modbus](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/modbus) [交叉编译] - 是用 C 语言编写的第三方Modbus库来实现modbus通讯，支持 RTU（串行）和 TCP（以太网）通信模式，可根据 Modbus 协议发送和接收数据
- [double-conversion](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/double-conversion) [交叉编译] - 用于IEEE高效二进制-十进制和十进制-二进制转换
- [busybox](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/busybox) [GN编译]- 将许多常见UNIX实用程序的微小版本组合成一个小型可执行文件
- [hunspell](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/hunspell) [交叉编译] - 是一个免费的拼写检查器和形态分析器库和命令行工具
- [libtommath](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/libtommath) [交叉编译] - 是一个完全用C语言编写的免费开源可移植数字理论多精度整数（MPI）库

- [bctoolbox](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/bctoolbox) [交叉编译] - 通信软件使用的一些实用库，像belle-sip、mediastreamer2和liblinphone
- [boost](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/boost) [交叉编译] - 是为C++语言标准库提供扩展的一些C++程序库的总称
- [ceres-solver](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/ceres-solver) [交叉编译] - 是一个由Google开发的开源C++库，用于解决具有边界约束和非线性最小二乘问题的优化问题，以及一般无约束优化问题
- [diff-match-patch-cpp-stl](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/diff-match-patch-cpp-stl) [交叉编译] - 是一个多种语言的高性能库，可操作纯文本
- [fmt](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/fmt) [交叉编译] - 是一个开源格式库，可提供C stdio和C ++ iostreams的快速安全替代品
- [gflags](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/gflags) [交叉编译] - 是一种命令行解析工具，主要用于解析用命令行执行可执行文件时传入的参数
- [libarchive](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libarchive) [交叉编译] - 项目开发了一种便携式，高效的C库，可以以各种格式读取和编写流库。它还包括使用libarchive库的常见功能包括，tar,cpio 和 zcat 命令行工具的实现
- [libffi](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libffi) [交叉编译] - 是一个开源库，提供了一种通用的调用外部函数的机制,允许程序在运行时动态地调用和执行编译时未知的、以及无法预先绑定的函数,支持多种编程语言,可以优化调用函数的性能
- [libusb](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libusb) [交叉编译] - 是一个 C 库，提供对 USB 设备的通用访问。它旨在供开发人员用来促进与 USB 硬件通信的应用程序的生成
- [lpeg](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lpeg) [交叉编译] - 是一个供lua使用的基于 Parsing Expression Grammars 的模式匹配库
- [luv](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/luv) [交叉编译]- 是一个用于lua的libuv裸绑定的库，使libuv可用于lua脚本
- [mythes](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/mythes) [交叉编译] - 是一个简单的词库，使用结构化的具有二进制搜索的文本数据文件和索引文件查找单词和短语并返回词性、意义和同义词
- [openldap](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openldap) [交叉编译] - 是一个开源的实现LDAP协议的软件套件，LDAP是一种用于访问和维护分布式目录信息的协议，而OpenLDAP提供了一套工具和库，使用户能够构建和管理LDAP服务器
- [tcl](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tcl) [交叉编译] - 是一种解释语言，也是该语言的一种非常便携的解释器
- [tesseract](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tesseract) [交叉编译] - 是一个OCR引擎，提供库函数和命令行工具
- [zbar](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/zbar) [交叉编译] - 是一个条形码和二维码解析的库
- [zxing-cpp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/zxing-cpp) [交叉编译] - 是一个二维码生成和解析的库
- [libcap](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libcap) [交叉编译] - 的功能是用于管理进程的权限和权限限制
- [qpdf](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/qpdf) [交叉编译] - 是一个命令行工具和 C++ 库，用于对 PDF 文件执行内容保留转换
- [libgphoto2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/libgphoto2) [交叉编译] - 是一个功能强大的库，用于通过应用程序访问和控制数字相机

### <a name="社区共建"></a>社区共建
#### <a name="JS-JS/ArkTS"></a>JS/ArkTS语言
##### <a name="UI-自定义控件JS/ArkTS"></a>UI
- [ohos_easyUI](https://gitee.com/openharmony-sig/ohos_easyui) - easyui是一套基于ArkTS语言开发的轻量、可靠的移动端组件库，它是对OpenHarmoney ArkUI进行深度定制的组件框架。 ohos_easyUI可扩展性较强，可以基于源码进行二次开发，修改原有组件以及新增部分组件，以满足具体项目的开发需求。 该框架适用大部分OpenHarmony 应用的开发，能够更加完善OpenHarmony 的应用开发能力，使我们的应用开发更加简单。
##### <a name="动画JS/ArkTS"></a>动画
##### <a name="网络-JS/ArkTS"></a>网络
##### <a name="图片JS/ArkTS"></a>图片
##### <a name="多媒体JS/ArkTS"></a>多媒体
##### <a name="数据存储JS/ArkTS"></a>数据存储
##### <a name="文件数据JS/ArkTS"></a>文件数据与传输
- [lyrics](https://gitee.com/wedatahub/lom_lyrics) - lyrics,是一个用于解析LRC文件和同步歌词的JavaScript库。
##### <a name="安全-JS/ArkTS"></a>安全
##### <a name="工具JS/ArkTS"></a>工具
- [isrc_fuse.js](https://gitee.com/pommejason/isrc_fuse.js) - Fuse.js是一款轻量级的JavaScript模糊搜索库，提供了模糊搜索和搜索排序功能。
- [eftool](https://gitee.com/yunkss/ef-tool) - eftool是一款基于ArkTS语言开发的轻量、可靠的、高效的工具包是基于OpenHarmony定制并封装了常用工具类如字符串、正则、加解密、分页、数据转换等和常用UI组件Toast,Dialog,Cascade等,提供一系列快捷操作方法。
##### <a name="其他JS/ArkTS"></a>其他
#### <a name="三方组件C_CPP"></a>C/C++语言
##### <a name="UI-自定义控件JS/ArkTS"></a>UI
##### <a name="动画JS/ArkTS"></a>动画
##### <a name="网络-JS/ArkTS"></a>网络
##### <a name="图片JS/ArkTS"></a>图片
##### <a name="多媒体JS/ArkTS"></a>多媒体
##### <a name="数据存储JS/ArkTS"></a>数据存储
##### <a name="文件数据JS/ArkTS"></a>文件数据与传输
##### <a name="安全-JS/ArkTS"></a>安全
##### <a name="工具JS/ArkTS"></a>工具
##### <a name="其他JS/ArkTS"></a>其他
